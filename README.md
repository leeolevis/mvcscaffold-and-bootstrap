MVCScaffold-And-Bootstrap
=========================

这是用MVCScaffold和Bootstrap混合搭建的一个后台框架，可以让你快速的构建UI、Controller和Repository的代码
<img src="http://pic.yupoo.com/leeolevis/CO7htGX2/medish.jpg"></img>

# Nuget Install-package #
<ol>
<li>Install-package MVCScaffold</li>
<li>Install-package PageList.MVC</li>
<li>Install-package Twitter Bootstrap for ASP.Net MVC 4</li>
</ol>

# PMC Usage #

<ol>
<li>执行Enable-Migrations命令启用数据库迁移</li>
<li>执行Add-Migration Initialize去初始化数据库</li>
<li>执行Update-Database去生成或更新数据库</li>
<li>执行Scaffold Controller Role -Repository -Forece生成代码</li>
</ol>

# 为雅安祈福，为四川祈福 #
